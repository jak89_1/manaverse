#!/usr/bin/env bash
# Copyright (C) 2021      The Mana World Development Team

DIR=`pwd`
new_small_version="$1.$2.$3.$4"
new_large_version="$(printf "%02d.%02d.%02d.%02d" $1 $2 $3 $4)"
new_date=`date +%Y-%m-%d`

[[ "$#" -ne 4 ]] && echo -e "wrong format! usage: $0 <MAJOR> <MINOR> <RELEASE> <BUILD>\n example: $0 2 4 1 1" && exit 1
[[ ! -d "$DIR/src" ]] && echo "you need to execute this file from repository base directory" && exit 1

# src/main.h
sed -i "s/\(#define SMALL_VERSION \"\).*\(\"\)/\1$new_small_version\2/g" ./src/main.h
sed -i "s/\(#define CHECK_VERSION \"\).*\(\"\)/\1$new_large_version\2/g" ./src/main.h

# README/README[.md|.txt]
sed -i "s/\(Version: \).*\(        Date: \).*\(\)/\1$new_small_version\2\3$new_date/g" ./README
sed -i "s/\(Version: \).*\(        Date: \).*\(\)/\1$new_small_version\2\3$new_date/g" ./README.md
sed -i "s/\(Version: \).*\(        Date: \).*\(\)/\1$new_small_version\2\3$new_date/g" ./README.txt

# build/[packevol|packtmw|packwin]
sed -i "s/\(-DPRODUCT_VERSION=\"\).*\(\" \\\)/\1$new_small_version\2/g" ./build/packevol
sed -i "s/\(-DPRODUCT_VERSION=\"\).*\(\" \\\)/\1$new_small_version\2/g" ./build/packtmw
sed -i "s/\(-DPRODUCT_VERSION=\"\).*\(\" \\\)/\1$new_small_version\2/g" ./build/packwin

# configure.ac
sed -i "s/\(AC_INIT(\[ManaVerse\], \[\).*\(\], \[jak1@themanaworld.org\], \[manaverse\])\)/\1$new_small_version\2/g" ./configure.ac

echo "src/main.h -> `git diff src/main.h`"
echo "README -> `git diff README`"
echo "README.md -> `git diff README.md`"
echo "README.txt -> `git diff README.txt`"
echo "build/packevol -> `git diff build/packevol`"
echo "build/packtmw -> `git diff build/packtmw`"
echo "build/packwin -> `git diff build/packwin`"
echo "configure.ac -> `git diff configure.ac`"

sed -i "1s/^/$new_date New release $new_small_version\n\n\n/" ChangeLog
# TODO: use commitlog to last taged commit (that can end in a mess!)
#       insteed just open the file on line 2 with vim
vim +2 ChangeLog

#git commit -am "Change version to $new_small_version"

exit 0